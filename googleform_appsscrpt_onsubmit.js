//This script can be used in Google Apps Script.
//After creating a form, go to the associated spreasheet, select Add-Ons, select Apps Script
//You can copy this script, change it to match to your form and don't forget to add a trigger. It's easy, check the Google Documentation.

function onSubmitForm(e) {

//Test Values

  var debug_e = {
    authMode:  e.authMode,  
    namedValues: e.namedValues,
  }
  console.log({message: 'onFormSubmit() Event Object', eventObject: debug_e});
//check the log to see the exact value you get, then you can start working

/* <== Remove this to uncomment to start modify the script, testing and using it. Enjoy !

//Elements for the CSV File Name
  //Date
  var rightNow = new Date();
  var res = rightNow.toISOString().slice(0,10).replace(/-/g,"");
  //Variable from the Form (example)
  var something01 = e.namedValues[ 'Value' ];
  var something02 = e.namedValues[ 'Value' ];
  var something03 = e.namedValues[ 'Value' ];

  //Questions Value of the Form
  //We are going to create arrays in order to create a csv file.
  //The result of the form is never in the same order, so you have to do it yourself. It correspond to the title of your questions in the forms.
  var values = ['Value01', 'Value02', 'Value03', 'Value04']

  //Creating the Second Row we want to create
  var csvFileRow2 = []
  for(value of values) {
    csvFileRow2 += e.namedValues[ value ] + ";";
  }

  //Transform csvFileRow2 into an array and adding the elements that doesn't exist in the form
  var arrCSVFileRow2 = csvFileRow2.split(";");
  arrCSVFileRow2.pop(); //remove the last column, which is empty because of the loop before
  console.log(arrCSVFileRow2);

//Creating the CSV File
  //Creating an array of array
  elements = [[values], [arrCSVFileRow2]];
  //Join the two array in elements  
  var csvFile = "";
  elements.forEach(function(row) {
    csvFile += row.join(',');
    csvFile += "\n";
  });

  //Creating the CSV file in specific Drive Folder
  //Select the folder
  var dir = DriveApp.getFolderById('id of the folder where you want the file');
  //Create the file
  var idCsvFile = dir.createFile(res + "_", csvFile, MimeType.CSV).getId();
  //Get the ID of the file for send by email
  var dCsvFile = DriveApp.getFileById(idCsvFile);



//Send the CSV File by email using Gmail
  var email = "example@test.com"
  GmailApp.sendEmail(email, "Title of your message", "Text of the email", {attachments: [dCsvFile.getAs(MimeType.CSV)]}); //you have several options for GmailApp function, check the documentation.

  /*

//Boucle for fullfilling the two arrays
  for(let field in e.namedValues) {
    csvFileRow1 += field + ",";
    csvFileRow2 += e.namedValues[field].toString() + ",";
  }

*/

    

}


        
